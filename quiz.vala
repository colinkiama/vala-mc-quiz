/**
 * A simple 4 choice multiple choice quiz.
 */
public class Quiz : Object {
	string[] keywords = null; // will resize
	string[] definitions = null; // will resize
	
	Rand random = new Rand (); // the RNG
	int question = 1; // current question
	int right = 0; // the number of right answers
	int wrong = 0; // the number of wrong answers
	bool running = true; // whether to keep asking questions
	
	int[] alreadyAnswered = null; // prevent duplication of keywords by storying already answerded ones
	int alreadyAnsweredSpot = 0;
	
	/**
	 * Loop which prints the question, prints the choices, gets input, and
	 * determines whether the player won, then goes onto the next round.
	 */
	public Quiz () {
		load_definitions ();
	
		while (running) {
			int answer = get_new_possible_choice (alreadyAnswered);
			int answerSpot = get_possible_spot ();
			int input = 0;
			
			stdout.printf ("QUESTION %d:\n", question);
			stdout.printf ("    Define %s:\n", keywords[answer]); // tell what you are asking for
			print_choices (answer, answerSpot);
			
			do {
				stdout.printf (">>");
				string line = stdin.read_line ();
				int choice = line[0];
				input = get_choice (choice);
				if (input == -2) Process.exit (0); // press Q to quit at any time
			} while (input == -1);
			
			if (input == answerSpot) { // TODO print winnings
				right++;
				stdout.printf ("CORRECT!\n");
				
				if (alreadyAnswered == null) {
					alreadyAnswered = new int[1];
				} else {
					alreadyAnswered.resize (alreadyAnsweredSpot + 1);
				}
				
				alreadyAnswered[alreadyAnsweredSpot] = answer;
				alreadyAnsweredSpot++;
			} else {
				wrong++;
				stdout.printf ("WRONG!!!\n");
			}
			stdout.printf ("RIGHT: %d | WRONG: %d | GRADE: %f\n\n", right, wrong, (float)right / question * 100);
			
			question++;
		}
	}
	
	/**
	 * Loads all the keywords and definitions defined in keywords.txt.
	 */
	private void load_definitions () {
		FileStream stream = FileStream.open ("keywords.txt", "r");
		string line = "";
		int spot = 0;
		
		assert (stream != null); // I guess this works...
		
		while ((line = stream.read_line ()) != null) {
			if (keywords == null) {
				keywords = new string[1];
				definitions = new string[1];
			} else {
				keywords.resize (spot + 1);
				definitions.resize (spot + 1);
			}
			
			keywords[spot] = line;
			definitions[spot] = stream.read_line ();
			spot ++;
		}
	}
	
	/**
	 * Prints the four choices, ensuring that the correct answer is one of them
	 * and that each possible choice is unique.
	 */
	private void print_choices (int answer, int answerSpot) {
		int[] chosenAnswers = new int[1]; // we'll resize this as we go
		int chosenAnswersSpot = 1; // where to add to chosenAnswers array
		
		chosenAnswers[0] = answer; // make sure the correct answer isn't chosen
		
		for (int i = 0; i < 4; i++) {
			if (i == answerSpot) {
				stdout.printf ("     %c) %s\n" , get_letter (i) , definitions[answer] );
			} else {
				int num = get_new_possible_choice (chosenAnswers);
				chosenAnswers.resize (chosenAnswersSpot + 1);
				chosenAnswers[chosenAnswersSpot] = num;
				chosenAnswersSpot++;
				stdout.printf ("     %c) %s\n" , get_letter (i) , definitions[num] );
			}
		}
	}
	
	/**
	 * Given an integer, return what the corresponding letter for that answer
	 * spot is.
	 */
	private char get_letter (int num) {
		switch (num) {
			case 0: return 'A';
			case 1: return 'B';
			case 2: return 'C';
			case 3: return 'D';
			default: return 'X';
		}
	}
	
	/**
	 * Given an int representation of a char figure out which choice it
	 * corresponds to.
	 */
	private int get_choice (int c) {
		switch (c) {
			case 'A':
			case 'a':
				return 0;
			case 'B':
			case 'b':
				return 1;
			case 'C':
			case 'c':
				return 2;
			case 'D':
			case 'd':
				return 3;
			case 'Q':
			case 'q':
				return -2;
			default:
				return -1;
		}
	}
	
	/**
	 * Returns a number from 0 to keywords.length-1 (inclusive), all the
	 * possible keywords.
	 */
	private int get_possible_choice () {
		return random.int_range (0, keywords.length);
	}
	
	/**
	 * Returns a number from 0 to 3 (inclusive); there are only 4 spots in a
	 * multiple choice quiz.
	 */
	private int get_possible_spot () {
		return random.int_range (0, 4);
	}
	
	/**
	 * Given an array of already displayed choices, generate one that is unique.
	 */
	private int get_new_possible_choice (int[] chosenAnswers) {
		int ret = 0;
		do {
			ret = get_possible_choice ();
			if (chosenAnswers == null) return ret;
		} while (ret in chosenAnswers);
		
		return ret;
	}
	
	/**
	 * The entry point; starts the quiz.
	 */
	public static int main (string[] args) {
		new Quiz ();
		return 0;
	}

}
